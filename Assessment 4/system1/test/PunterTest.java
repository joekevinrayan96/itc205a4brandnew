import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PunterTest {
	
	private static List<Die> setUpDice(){
		Die d1=new Die();
		Die d2=new Die();
		Die d3=new Die();
		List<Die> dice=new ArrayList<>(Arrays.asList(d1, d2, d3));
		
		return dice;
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	@Test
	void testBalanceExceedsLimitBy() {
		Punter punter=new Punter("Kevin",100,10);
		assertTrue(punter.balanceExceedsLimitBy(90));
		
	}

	

	

	/*@Test
	void testReceiveWinnings() {
		Punter punter=new Punter("Kevin",100,10);
		
		int matches =0;
		Die die=new Die();
		die.roll();
		int currentBet=20;
		punter.placeBet(currentBet);
		Face pick=die.getFace();
		
		if(die.getFace().equals(pick)) {
			matches += 1;
		}
		int winnings = matches * currentBet;
		
		if(matches==1) {
			punter.returnBet();
			assertEquals(100,punter.getBalance());
			
			punter.receiveWinnings(winnings);
			assertEquals(120,punter.getBalance());
		}
	}*/

	/*@Test
	void testLoseBet() {
		Punter punter =new Punter("Kevin", 100, 10);
		
		int matches=0;
		Die die=new Die();
		die.roll();
		int currentBet=20;
		punter.placeBet(currentBet);
		
		Face pick=Face.getByIndex(0);
		
		matches = 0;
		
		int wins=matches*currentBet;
		
		if(matches==1) {
			punter.returnBet();
			assertEquals(100, punter.getBalance());
			
			punter.receiveWinnings(wins);
			assertEquals(120,punter.getBalance());
		}
		else
		{
			punter.loseBet();
			assertEquals(80,punter.getBalance());
		}
		
	}*/

	/*@Test
	void testToString() {
		fail("Not yet implemented");
	}*/

}
